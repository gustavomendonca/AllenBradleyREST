﻿namespace AllenBradleyREST.PLC
{
    public enum CodigoErro
    {
        SEM_ERRO,
        ERRO_GENERICO,
        FALHA_COMUNICACAO_PLC,
        TAG_NAO_EXISTE
    }

    public enum Status
    {
        OK,
        NOK

    }

    public class Resultado
    {
        public Resultado()
        {
            CodigoErro = CodigoErro.SEM_ERRO;
        }

        public string Valor { get; set; }
        public CodigoErro CodigoErro { get; set; }
        public string Erro { get; set; }
    }
}
