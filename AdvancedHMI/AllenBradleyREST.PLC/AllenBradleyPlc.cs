﻿using System;

namespace AllenBradleyREST.PLC
{
    public class AllenBradleyPlc
    {
        private AdvancedHMIDrivers.EthernetIPforCLXCom ethernetIPforCLXCom;

        public AllenBradleyPlc(string IpPlc, int Porta, int CpuSlot, int Timeout)
        {
            ethernetIPforCLXCom = new AdvancedHMIDrivers.EthernetIPforCLXCom
            {
                IPAddress = IpPlc,
                Port = Porta,
                ProcessorSlot = CpuSlot,
                Timeout = Timeout
            };
        }

        public Resultado EscreveTag(string tag, bool valor)
        {
            Resultado ret = new Resultado();
            try
            {
                byte val = (byte)((valor) ? 1 : 0);
                ethernetIPforCLXCom.Write(tag, val);
                ret.Valor = val.ToString();
            }
            catch (Exception ex)
            {
                ret.CodigoErro = TraduzCodigoErroPelaMensagem(ex.Message); ret.Erro = ex.Message;
                ret.Erro = ex.Message;
            }
            return ret;
        }

        public Resultado LeTag(string tag)
        {
            Resultado ret = new Resultado();
            try
            {
                string val = ethernetIPforCLXCom.Read(tag);
                ret.Valor = val;
            }
            catch (Exception ex) {
                ret.CodigoErro = TraduzCodigoErroPelaMensagem(ex.Message);
                ret.Erro = ex.Message;
            }
            return ret;
        }

        private CodigoErro TraduzCodigoErroPelaMensagem(string mensagem)
        {
            switch (mensagem)
            {
                case "No response from PLC.":
                    return CodigoErro.FALHA_COMUNICACAO_PLC;
                //TODO: verificar mensagem real
                case "tag nao existe.":
                    return CodigoErro.TAG_NAO_EXISTE;
                default:
                    return CodigoErro.ERRO_GENERICO;
            }
        }

        ~AllenBradleyPlc()
        {
            try
            {
                ethernetIPforCLXCom.CloseConnection();
            }
            catch (Exception)
            { }
            ethernetIPforCLXCom.Dispose();
        }
    }
}
