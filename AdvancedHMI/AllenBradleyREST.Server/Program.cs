﻿using AllenBradleyREST.Service;
using System;
using System.ServiceModel.Web;

namespace AllenBradleyREST.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                // encerra programa com dódigo de falha
                throw new Exception("Programa executado sem parâmetros");
            }
            
            PlcConfig config = new PlcConfig
            {
                Ip = args[0],
                Porta = int.Parse(args[1]),
                SlotCpu = int.Parse(args[2]),
                Timeout = int.Parse(args[3])
            };

            AllenBradleyRESTService AllenBradleyRESTService = new AllenBradleyRESTService(config);

            WebServiceHost _serviceHost = new WebServiceHost(AllenBradleyRESTService, new Uri("http://localhost:7878/plc"));
            _serviceHost.Open();
            Console.WriteLine("Servidor iniciado");
            Console.ReadKey();
            _serviceHost.Close();
        }
    }
}
