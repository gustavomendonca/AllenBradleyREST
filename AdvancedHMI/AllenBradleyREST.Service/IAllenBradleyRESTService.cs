﻿using AllenBradleyREST.PLC;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace AllenBradleyREST.Service
{
    [ServiceContract(Name = "AllenBradleyRESTService")]
    public interface IAllenBradleyRESTService
    {
        [OperationContract]
        [WebGet(UriTemplate = Rotas.LeTag, BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Resultado LeTag(string NomeTag);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = Rotas.EscreveTag, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        Resultado EscreveTag(string NomeTag, bool valor);
    }
}
