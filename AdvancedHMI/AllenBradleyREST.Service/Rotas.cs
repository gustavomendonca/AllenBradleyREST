﻿namespace AllenBradleyREST.Service
{
    public static class Rotas
    {
        public const string EscreveTag = "/escreve/{NomeTag}";
        public const string LeTag = "/le/{NomeTag}";
        public const string Inicia = "/inicia";
        public const string Estado = "/estado";
    }
}
