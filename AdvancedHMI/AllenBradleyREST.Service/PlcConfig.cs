﻿namespace AllenBradleyREST.Service
{
    public class PlcConfig
    {
        public string Ip { get; set; }
        public int Porta { get; set; }
        public int SlotCpu { get; set; }
        public int Timeout { get; set; }
    }
}
