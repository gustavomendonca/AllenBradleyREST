﻿using AllenBradleyREST.PLC;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace AllenBradleyREST.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single, IncludeExceptionDetailInFaults = true)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AllenBradleyRESTService : IAllenBradleyRESTService
    {
        private AllenBradleyPlc plc;

        public AllenBradleyRESTService(PlcConfig Config)
        {
            plc = new AllenBradleyPlc(Config.Ip, Config.Porta, Config.SlotCpu, Config.Timeout);
        }
        
        public Resultado LeTag(string NomeTag)
        {
            return plc.LeTag(NomeTag);
        }

        public Resultado EscreveTag(string NomeTag, bool valor)
        {
            return plc.EscreveTag(NomeTag, valor);           
        }
    }
}
