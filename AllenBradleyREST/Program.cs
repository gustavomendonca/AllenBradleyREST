﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;

namespace AllenBradleyREST
{
    class Program {
        // Configuração das rotas (endpoints)
        const string BASE_URL = "http://localhost:7878/plc/";
        const string WRITE_ENDPOINT = "escreve/";
        const string READ_ENDPOINT = "le/";
        const string SETUP_ENDPOINT = "setup/";

        static void Main(string[] args) {
            Console.WriteLine("ALLEN BRADLEY REST WEB SERVER");

            // Cria endpoints
            WebServer write = new WebServer(WriteResponse, BASE_URL + WRITE_ENDPOINT);
            write.Run();

            WebServer read = new WebServer(ReadResponse, BASE_URL + READ_ENDPOINT);
            read.Run();

            WebServer setup = new WebServer(SetupResponse, BASE_URL + SETUP_ENDPOINT);
            setup.Run();

            // Mantém a aplicação aberta
            Console.ReadKey();

            // Para de ouvir todos os endpoints
            write.Stop();
            read.Stop();
            setup.Stop();
        }

        /// <summary>
		/// Rota de escrita de tags
		/// </summary>
		/// <param name="request">Objeto de resposta da requisição</param>
		/// <param name="statusCode">Ponteiro para a variável que receberá o código de status da requisição</param>
		/// <param name="contentType">Ponteiro para a variável que contém o tipo de resposta da requisição (OBS: modifique seu valor apenas se necessário)</param>
		/// <returns>Conteúdo a ser escrito no OutputStream da resposta, informando se a tag foi escrita com sucesso</returns>
		public static string WriteResponse(HttpListenerRequest request, out int statusCode, ref string contentType)
        {
            string postBody;

            // Instâncias de Stream devem ser criadas com using para forçar a chamada do método Dispose após utiliza-las, liberando memória
            using (StreamReader reader = new StreamReader(request.InputStream, request.ContentEncoding))
            {
                postBody = reader.ReadToEnd();
            }

            Dictionary<string, object> postData;
            try
            {
                postData = (Dictionary<string, object>)(new JavaScriptSerializer()).DeserializeObject(postBody);
            }
            catch (Exception err)
            {
                statusCode = 400;
                return "Bad Request";
            }

            if (request.HttpMethod != "POST" || !postData.ContainsKey("tag") || !postData.ContainsKey("valor"))
            {
                statusCode = 400;
                return "Bad Request";
            }

            bool valor;
            string tag;

            postData.TryGetValue("tag", out object temp);
            tag = (string)temp;

            // Recupera o campo valor de forma segura
            try
            {
                postData.TryGetValue("valor", out temp);
                valor = (bool)temp;
            }
            catch (Exception err)
            {
                statusCode = 400;
                return "Bad Request";
            }

            // Executa a ação desse endpoint
            PLC.Resultado result = PLC.Escreve(tag, valor);

            // Processa resultado da ação
            if (result.status == PLC.Resultado.STATUS.OK)
            {
                statusCode = 200;
                return "Success";
            }
            else if (result.status == PLC.Resultado.STATUS.BAD)
            {
                statusCode = 400;
                return "Bad Request";
            }

            statusCode = 500;
            contentType = "application/json";
            return "{" + string.Format("\"erro\":\"{0}\"", (!string.IsNullOrEmpty(result.erro) ? result.erro : "Desconhecido")) + "}";
        }

        /// <summary>
        /// Rota de leitura de tags
        /// </summary>
        /// <param name="request">Objeto de resposta da requisição</param>
        /// <param name="statusCode">Ponteiro para a variável que receberá o código de status da requisição</param>
        /// <param name="contentType">Ponteiro para a variável que contém o tipo de resposta da requisição (OBS: modifique seu valor apenas se necessário)</param>
        /// <returns>Conteúdo a ser escrito no OutputStream da resposta contendo o valor da tag</returns>
        public static string ReadResponse(HttpListenerRequest request, out int statusCode, ref string contentType)
        {
            string tag;
            if (request.HttpMethod != "GET" || request.QueryString == null || string.IsNullOrEmpty(tag = request.QueryString.Get("tag")))
            {
                statusCode = 400;
                return "Bad Request";
            }

            // Executa a ação desse endpoint
            PLC.Resultado result = PLC.Le(tag);

            // Processa resultado da ação
            if (result.status == PLC.Resultado.STATUS.OK)
            {
                statusCode = 200;
                return result.valor;
            }
            else if (result.status == PLC.Resultado.STATUS.BAD)
            {
                statusCode = 400;
                return "Bad Request";
            }

            statusCode = 500;
            contentType = "application/json";
            return "{" + string.Format("\"erro\":\"{0}\"", (!string.IsNullOrEmpty(result.erro) ? result.erro : "Desconhecido")) + "}";
        }

        /// <summary>
        /// Rota de configuração da comunicação com o PLC
        /// </summary>
        /// <param name="request">Objeto de resposta da requisição</param>
        /// <param name="statusCode">Ponteiro para a variável que receberá o código de status da requisição</param>
        /// <param name="contentType">Ponteiro para a variável que contém o tipo de resposta da requisição (OBS: modifique seu valor apenas se necessário)</param>
        /// <returns>Conteúdo a ser escrito no OutputStream da resposta, informando se as configurações foram salvas</returns>
        public static string SetupResponse(HttpListenerRequest request, out int statusCode, ref string contentType)
        {
            string postBody;

            // Instâncias de Stream devem ser criadas com using para forçar a chamada do método Dispose após utiliza-las, liberando memória
            using (StreamReader reader = new StreamReader(request.InputStream, request.ContentEncoding))
            {
                postBody = reader.ReadToEnd();
            }

            Dictionary<string, object> postData;
            try
            {
                postData = (Dictionary<string, object>)(new JavaScriptSerializer()).DeserializeObject(postBody);
            }
            catch (Exception err)
            {
                statusCode = 400;
                return "Bad Request";
            }

            if (request.HttpMethod != "POST" || postData == null || !postData.ContainsKey("ip") || !postData.ContainsKey("porta") || !postData.ContainsKey("cpuSlot") || !postData.ContainsKey("timeout"))
            {
                statusCode = 400;
                return "Bad Request";
            }

            int porta, cpuSlot, timeout;
            string ip;

            postData.TryGetValue("ip", out object temp);
            ip = (string)temp;

            try
            {
                postData.TryGetValue("porta", out temp);
                porta = (int)temp;

                postData.TryGetValue("cpuSlot", out temp);
                cpuSlot = (int)temp;

                postData.TryGetValue("timeout", out temp);
                timeout = (int)temp;
            }
            catch (Exception err)
            {
                statusCode = 400;
                return "Bad Request";
            }

            // Executa a ação desse endpoint
            PLC.Resultado result = PLC.Setup(ip, porta, cpuSlot, timeout);

            // Processa resultado da ação
            if (result.status == PLC.Resultado.STATUS.OK)
            {
                statusCode = 200;
                return "Success";
            }
            else if (result.status == PLC.Resultado.STATUS.BAD)
            {
                statusCode = 400;
                return "Bad Request";
            }

            statusCode = 500;
            contentType = "application/json";
            return "{" + string.Format("\"erro\":\"{0}\"", (!string.IsNullOrEmpty(result.erro) ? result.erro : "Desconhecido")) + "}";
        }
    }
}
