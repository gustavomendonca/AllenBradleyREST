﻿using System;

namespace AllenBradleyREST
{
    static class PLC
    {
        /// <summary>
        /// Objeto de resultado para comunicação com o serviço WEB
        /// </summary>
        public struct Resultado
        {
            public enum STATUS { OK, BAD, ERRO }

            public Resultado(STATUS status, string valor, string erro = null)
            {
                this.valor = valor;
                this.status = status;
                this.erro = erro;
            }

            public string erro;
            public string valor;
            public STATUS status;
        }

        /// <summary>
        /// Escreve um valor na tag especificada
        /// </summary>
        /// <param name="tag">TAG do PLC</param>
        /// <param name="valor">Valor a ser escrito</param>
        /// <returns>Resultado da escrita</returns>
        public static Resultado Escreve(string tag, bool valor)
        {
            Console.WriteLine(string.Format("Escrever '{0}' na tag '{1}'", valor, tag));
            return new Resultado(Resultado.STATUS.OK, tag);
        }

        /// <summary>
        /// Lê uma tag específica
        /// </summary>
        /// <param name="tag">TAG do PLC</param>
        /// <returns>Valor lido da tag</returns>
        public static Resultado Le(string tag)
        {
            Console.WriteLine(string.Format("Lê a tag '{0}'", tag));
            return new Resultado(Resultado.STATUS.OK, "Valor lido da tag.");
        }

        /// <summary>
        /// Configura a comunicação com o PLC
        /// </summary>
        /// <param name="ip">IP do PLC</param>
        /// <param name="porta">Porta de comunicação com o PLC</param>
        /// <param name="cpuSlot"></param>
        /// <param name="timeout">Tempo de expiração</param>
        /// <returns>Resultado das configurações</returns>
        public static Resultado Setup(string ip, int porta, int cpuSlot, int timeout)
        {
            Console.WriteLine(string.Format("Configuração:\n    IP: {0}\n    PORTA: {1}\n    CPU SLOT: {2}\n    TIMEOUT: {3}", ip, porta, cpuSlot, timeout));
            return new Resultado(Resultado.STATUS.OK, ip);
        }
    }
}