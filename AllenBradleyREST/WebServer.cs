﻿using System;
using System.Net;
using System.Text;
using System.Threading;

namespace AllenBradleyREST
{
    public class WebServer {
        private readonly HttpListener _listener = new HttpListener();
        private readonly ResponderMethod _responderMethod;

        public delegate string ResponderMethod(HttpListenerRequest request, out int statusCode, ref string contentType);

        public WebServer(string[] prefixes, ResponderMethod method)
        {
            if (!HttpListener.IsSupported)
                throw new NotSupportedException("Needs Windows XP SP2, Server 2003 or later.");

            // URI prefixes are required, for example "http://localhost:8080/index/"
            if (prefixes == null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");

            foreach (string s in prefixes)
                _listener.Prefixes.Add(s);

            _responderMethod = method ?? throw new ArgumentException("method");
            _listener.Start();
        }

        public WebServer(ResponderMethod method, params string[] prefixes) : this(prefixes, method) { }

        public void Run()
        {
            ThreadPool.QueueUserWorkItem((o) => {
                Console.WriteLine("Webserver running...");
                try
                {
                    while (_listener.IsListening)
                    {
                        ThreadPool.QueueUserWorkItem((c) => {
                            var ctx = c as HttpListenerContext;
                            try
                            {
                                string contentType = "text/html";
                                string rstr = _responderMethod(ctx.Request, out int statusCode, ref contentType);
                                byte[] buf = Encoding.UTF8.GetBytes(rstr);
                                ctx.Response.Headers["Content-Type"] = contentType;
                                ctx.Response.StatusCode = statusCode;
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                            catch { } // Suppress any exceptions
                            finally
                            {
                                // Always close the stream
                                ctx.Response.OutputStream.Close();
                            }
                        }, _listener.GetContext());
                    }
                }
                catch { } // Suppress any exceptions
            });
        }

        public void Stop()
        {
            _listener.Stop();
            _listener.Close();
        }
    }
}
